<!--
 * @Author: xuhua
 * @Date: 2023-02-17 11:24:12
 * @LastEditors: xuhua
 * @LastEditTime: 2023-02-17 11:49:41
 * @FilePath: /数字工作平台工具化测试_2/Readme.md
 * @Description: 
-->
# 使用 进行自动化测试编写

## 安装依赖
npm install --registry https://registry.npm.taobao.org


## 测试用例编写
1. 请在 specs 目录下建立测试用例，一个模块一个文件
2. 请在模块内完成该模块的全部功能测试用例
3. 可以通过 npm test 运行全部测试用例
4. 在 reports 查看测试结果和测试报告
5. 如果需要单用例执行，请使用vscode，并且安装 gauge 插件。






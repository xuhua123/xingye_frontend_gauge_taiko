/* globals gauge*/
"use strict";
const path = require("path");
const {
  openBrowser,
  write,
  closeBrowser,
  goto,
  press,
  screenshot,
  above,
  waitFor,
  click,
  checkBox,
  listItem,
  toLeftOf,
  link,
  text,
  into,
  $,
  textBox,
  evaluate,
} = require("taiko");
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === "true";

const name = require("../utils/name.js");

// beforeSuite(async () => {
//     await openBrowser({
//         headless: headless
//     })
// });

// afterSuite(async () => {
//     await closeBrowser();
// });

beforeScenario(async () => {
  await openBrowser({
    headless: headless,
    args: ["--start-fullscreen", "--start-maximized"],
  });
});

afterScenario(async () => {
  await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
  const screenshotFilePath = path.join(
    process.env["gauge_screenshots_dir"],
    `screenshot-${process.hrtime.bigint()}.png`
  );

  await screenshot({
    path: screenshotFilePath,
  });
  return path.basename(screenshotFilePath);
};

step("Add task <item>", async (item) => {
  await write(item, into(textBox("What needs to be done?")));
  await press("Enter");
});

step("View <type> tasks", async function (type) {
  await click(link(type));
});

step("Complete tasks <table>", async function (table) {
  for (var row of table.rows) {
    await click(checkBox(toLeftOf(row.cells[0])));
  }
});

step("Clear all tasks", async function () {
  await evaluate(() => localStorage.clear());
});

step("Open todo application", async function () {
  await goto("todo.taiko.dev");
});

step("Must not have <table>", async function (table) {
  for (var row of table.rows) {
    assert.ok(!(await text(row.cells[0]).exists(0, 0)));
  }
});

step("Must display <message>", async function (message) {
  assert.ok(await text(message).exists(0, 0));
});

step("Add tasks <table>", async function (table) {
  for (var row of table.rows) {
    await write(row.cells[0]);
    await press("Enter");
  }
});

step("Must have <table>", async function (table) {
  for (var row of table.rows) {
    assert.ok(await text(row.cells[0]).exists());
  }
});

step("打开登录页", async function () {
  await goto("http://49.235.224.111/");
});

step("输入用户名<username>", async function (username) {
  await write(username, into(textBox({ id: "username" })));
});

step("输入密码<password>", async function (password) {
  await write(password, into(textBox({ id: "password" })));
});

step("点击<text>按钮", async function (text) {
  await click($(`//*[text()="${text}"]`));
});

step("输入验证码", async function () {
  const code = await evaluate(() => document.querySelector(".code").innerText);
  await write(code, into(textBox({ id: "codeValue" })));
});

step("界面显示<text>", async function (text) {
  assert.ok(await $(`//*[text()="${text}"]`).exists());
});

step("等待<sec>秒", async function (sec) {
  await waitFor(sec * 1000);
});

step("在第<index>个输入框输入<text>", async function (index, text) {
  //待处理
  await write(text, into(textBox({ index: index })));
});

step("在占位符是<ps>的输入框输入<text>", async function (ps, text) {
  await write(text, into(textBox({ placeholder: ps })));
});

step("查询结果必须包含 <table>", async function (table) {
  for (var row of table.rows) {
    assert.ok(await text(row.cells[0]).exists());
  }
});

step("在占位符是<ps>的输入框点击选择日期范围<startdate>到<enddate>", async function (ps, startdate, enddate) {
  await click(textBox({ placeholder: ps }));
  await click($(`//*[@title="${startdate}"]`));
  await click($(`//*[@title="${enddate}"]`));
});

step("选择<date>日期范围", async function (date) {
  await click($(`//*[text()="${date}"]`));
});

step("在占位符是<text>的选择框点击选择<select>", async function (text, select) {
  await click(textBox({ placeholder: text }));
  await click($(`//*[@title="${select}"]`));
});
step("在类名为<className>的选择框点击选择<select>", async function (className, select) {
  await click(textBox({ class: className }));
  await click($(`//*[@title="${select}"]`));
});

step("点击第<index>个样式配置", async function (index) {
  await click($(`//*[@class="ant-btn ant-btn-link"][${index}]`));
});
step("点击<index>", async function (index) {
  await click($(`//*[@title="${index}"]`));
});

step("在类名是<className>的输入框输入<text>", async function (className, text) {
  await write(text, into(textBox({ class: className })));
});

step("点击类名为<className>的按钮", async function (className) {
  await click($(`//*[@class="${className}"]`));
});

step("选择颜色为<color>", async function (color) {
  await click($(`//*[@style="background-color: rgb(color);"]`));
});

// step("Must have <table>", async function (table) {
//     for (var row of table.rows) {
//         assert.ok(await text(row.cells[0]).exists());
//     }
// });
